import requests
from lxml import etree, builder
from io import BytesIO
from time import sleep

# url1 = "https://sipservicetest.azurewebsites.net/SIPService.asmx"  # url test
url1 = "https://sipserviceclientev6.azurewebsites.net/SIPService.asmx"
namespaces = {
    'xsi': "http://www.w3.org/2001/XMLSchema-instance",
    'xsd': "http://www.w3.org/2001/XMLSchema",
    'soapenv': "http://schemas.xmlsoap.org/soap/envelope/",
    'sip': "http://sip.net.co/"
}


def get_pay(data, terminal):

    soapenv = builder.ElementMaker(namespace=namespaces['soapenv'])
    element = builder.ElementMaker(namespace=namespaces['sip'])

    att_doc = builder.ElementMaker(nsmap=namespaces, namespace=namespaces['soapenv'])
    doc_xml = att_doc.Envelope(
        soapenv.Body(
            element.EscribirSolicitudPC(
                element.Terminal(terminal),
                element.Data(data),
                xmlns="http://sip.net.co",
            )
        )
    )
    xml_customer = etree.tostring(
        doc_xml,
        encoding='UTF-8',
        pretty_print=True,
        xml_declaration=True)

    headers = {
        # "Host": "sipservicetest.azurewebsites.net",
        "Host": "sipserviceclientev6.azurewebsites.net",
        "Content-Type": "text/xml; charset=utf-8",
        "Content-Length": str(len(xml_customer)),
        "SOAPAction": "http://sip.net.co/EscribirSolicitudPC",
    }
    response = requests.post(url1,  data=xml_customer, headers=headers, timeout=10)
    # file_ = open("/home/psk/redeban_test/escribirSolicitudV1.xml", "w")
    # file_.write(xml_customer.decode("utf-8"))
    # file_.close()
    # print(response, 'this is response')
    return response


def get_response_pay(terminal):

    soapenv = builder.ElementMaker(namespace=namespaces['soapenv'])
    element = builder.ElementMaker(namespace=namespaces['sip'])
    att_doc = builder.ElementMaker(nsmap=namespaces, namespace=namespaces['soapenv'])
    doc_xml = att_doc.Envelope(
        soapenv.Body(
            element.LeerRespuestaPC(
                element.Terminal(terminal),
                xmlns="http://sip.net.co",
            )
        )
    )
    xml_customer = etree.tostring(
        doc_xml,
        encoding='UTF-8',
        pretty_print=True,
        xml_declaration=True)

    headers = {
        # "Host": "sipservicetest.azurewebsites.net",
        "Host": "sipserviceclientev6.azurewebsites.net",
        "Content-Type": "text/xml; charset=utf-8",
        "Content-Length": str(len(xml_customer)),
        "SOAPAction": "http://sip.net.co/LeerRespuestaPC",
    }
    response = requests.post(url1, data=xml_customer, headers=headers, timeout=10)
    # file_ = open("/home/psk/redeban_test/leerRespuestaV1.xml", "w")
    # file_.write(xml_customer.decode("utf-8"))
    # file_.close()
    return response


def process_response(response):
    response_text = None
    if response.status_code == 200:
        xml_response = BytesIO(response.content)
        for event, element in etree.iterparse(xml_response):
            if etree.QName(element).localname == 'EscribirSolicitudPCResult' and element.text:
                response_text = element.text
            elif etree.QName(element).localname == 'LeerRespuestaPCResult' and element.text:
                response_text = element.text
    return response_text


def get_dict_response_pay(data):
    values = data.split(',')
    keys = [
        'respuesta', 'codigo_aprobacion', 'bin_tarjeta',
        'tipo_cuenta', 'franquicia', 'monto_transaccion', 'iva_transaccion',
        'base_devolucion', 'impuesto_consumo', 'recibo_datafono', 'num_cuotas',
        'consecutivo_transaccion', 'numero_terminal', 'codigo_establecimiento',
        'fecha', 'hora', 'num_bono'
    ]
    return {keys[i]: values[i] for i in range(len(values))}
